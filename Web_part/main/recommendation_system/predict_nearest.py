import pickle as pk
import numpy as np

with open('data/beer_data.pickle', 'rb') as f:
    beer_data = pk.load(f)

categories = list(beer_data[0])
positions = list(beer_data[5])


def pred_by_dists(good_beers, n_nearest, metric='sum_sq'):
    out = []
    for i in range(len(good_beers)):
        n = 0
        if n_nearest[i] <= 5:
            continue
        elif n_nearest[i] > 5:
            n += 1
            if n_nearest[i] > 7:
                n += 1
            if n_nearest[i] == 10:
                n += 1
        out += dists_calc(good_beers[i] - 1, n_nearest=n, metric=metric)
    return list(np.unique(out))


def dists_calc(beer_index, n_nearest=3, metric='sum_sq', position=None): # TODO: completely rewrite
    if position is None:
        position = positions[beer_index]

    cat = categories[0:beer_index] + categories[beer_index + 1:]
    pos = positions[0:beer_index] + positions[beer_index + 1:]
    res = []
    for i in range(len(pos)):
        if metric == "sum_sq":
            dist = (sum([(pos[i][x] - position[x]) ** 2 for x in range(len(pos[i]))])) ** 0.5
        elif metric == 'max_dist':
            dist = (max([abs(pos[i][x] - position[x]) for x in range(len(pos[i]))]))
        elif metric == 'city_dist':
            dist = (sum([abs(pos[i][x] - position[x]) for x in range(len(pos[i]))]))
        else:
            raise Exception("Unexpected metric value: " + metric)

        res.append([cat[i], dist])
    res = np.array(res)
    out = []
    for n in range(n_nearest):
        sub = res[res[:, 1].argmin()][0]
        out.append(sub)
        res[res[:, 1].argmin()] = ['kek', 'kek']
    return out
