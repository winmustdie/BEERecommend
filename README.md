# BEERecommend
![](https://gitlab.com/goto-ru/appliedprog/school-2017.10-spb/BEERecommend/raw/master/images/BEERecommend.png)
*******

# Как использовать Web_part
Требуется установить ряд библиотек:
1. django
2. django-widjet-tweaks
3. numpy
4. pickle

Для скачивания проекта и запуска localhost'а:
```bash
git clone https://gitlab.com/winmustdie/BEERecommend.git
cd BEERecommend/Web_part/
python3 manage.py runserver
```
